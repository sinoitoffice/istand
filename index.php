<?php
session_start();
session_destroy();

?>


<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>

	
			<nav id="nav">
				<ul class="container">
					<li><a href="" style="cursor:none;"></a></li>
					<li><a href="" style="cursor:none;"></a></li>
					<li><a href="" style="cursor:none;"></a></li>
					<li><a href="" style="cursor:none;"></a></li>
				</ul>
				
			</nav>


		<!-- Work -->
			<div class="wrapper style2">
				<article id="work">
					<header>
						<h2>Salone del Risparmio 2016</h2>
						<h3>Login</h3>
						
					</header>
					<form action="login.php" method="post"/>
					<div class="container">
						<div class="row">
							<div class="4u 12u(mobile) product">
								<section class="box style1">
								<?php
									if(isset($_GET['error'])) {
										$msg = "";
										if ($_GET['error'] == 1) $msg = "Login Fallito";
										
										
										echo "<div class='error' >".$msg."<br>Riprova</div>";
									}
									
									?>
									<div class="6u 12u(mobile)" style="margin:auto;">
												<input class="spazio" type="text" name="p1" id="p1" placeholder="username">
												<input class="spazio" type="password" name="p2" id="p2" placeholder="password">
												
											</div>
									<input type="submit" name="login" class="button small scrolly" value="login"/>
									
								</section>
							</div>
							
						</div>
					</div>
					</form>
					<?php include("webapp/app/footer.php"); ?>
				</article>
			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>