<?php
include("webapp/app/connessione.php");
include("webapp/app/function.php");
if (isset( $_SESSION['company']['id'])) { ?>

<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css?<?php echo time(); ?>" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
	<!-- Nav -->
		<?php include("webapp/app/menu.php"); ?>



		<!-- Work -->
			<div class="wrapper style2">
				<article id="work">
					<header>
						<h2><?php echo  $_SESSION['company']['name']; ?></h2>
						
					</header>
					<div class="container">
						<div class="row"> 
						
							<div class="4u 12u(mobile) ss">
								<article class="box style2">
									<a href="product.php" class="image featured"><img src="images/settings.png" alt="" style="width:128px;height:128px;margin:auto"/></a>
									<h3><a href="product.php">Configurazione prodotti</a></h3>
									
								</article>
							</div>
							<div class="4u 12u(mobile) ss">
								<article class="box style2">
									<a href="download.php" class="image featured"><img src="images/download.png" alt=""  style="width:128px;height:128px;margin:auto"/></a>
									<h3><a href="download.php">Scarico XLS</a></h3>
									
								</article>
							</div>
							
							
							
						</div>
					</div>
					<?php include("webapp/app/footer.php"); ?>
				</article>
			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
<?php 
$conn = null;

} else echo "<script>location.href = 'index.php';</script>";  ?>