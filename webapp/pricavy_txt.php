<?php
include("app/connessione.php");
include("app/function.php");

if (isset($_SESSION['company']['id'])) { 
if (isset($_GET['barcode'])) $barcode = $_GET['barcode'];
	else $barcode = "";

?>

<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		
	</head>
	<body style="padding:20px;">

	
				<?php include("app/menu_app.php"); ?>
<!-- testo -->





<!-- fine -->



		<!-- Work -->
			<div class="wrapper style2">
				<article id="work">
					<header>
						<h2><?php
						$d = 'Finalit� e modalit� del trattamento dei dati personali';
						echo utf8_encode($d)
						?>
						</h2>					
					</header>
	<div style="width:100%;text-align:right;"><a href="privacy.php?barcode=<?php echo $barcode; ?>">CHIUDI</a></div>				
					<div class="container">
						<div class="row" >
							<div class="4u 12u(mobile)" style="width:100%; text-align:left;color:#000;text-align:justify">
<?php

$str = 'Finalit� e modalit� del trattamento dei dati personali<br><br>Ai sensi dell�art. 13 del DLGS n. 
196/2003 (c.d. Codice della Privacy), nella Vostra qualit� di Interessati al trattamento, Vi informiamo
 che i dati personali da Voi forniti nella scheda di registrazione saranno trattati da Assogestioni e 
 da Assogestioni Servizi Srl, (rispettivamente  con sede legale a Roma in Via In Lucina n. 17 e a Milano
 in Via Andegari n. 18), in qualit� di contitolari del trattamento, con modalit� informatiche e/o cartacee 
 e comunque in modo da garantire la sicurezza, la protezione e la segretezza dei Vostri dati,  per le seguenti
 finalit�:<br><br>1)	per consentire l�iscrizione e la partecipazione dell�Interessato all�evento �Il Salone 
 del Risparmio � Ed. 2016� e ad eventi ed incontri connessi a detta manifestazione;<br>2)	previo specifico 
 consenso dell�Interessato, per consentire ai Contitolari la realizzazione di finalit� di marketing, ivi incluse 
 l�invio di materiale promozionale ed informativo; l�invio delle newsletter relative all�evento �Il Salone del 
 Risparmio � Ed. 2016�; l�invio di inviti ad eventi ed iniziative dei contitolari o da questi gestite; il 
 compimento di studi e ricerche di mercato.<br>3)	previo specifico consenso dell�Interessato, per consentire 
 ai Contitolari la comunicazione e/o la cessione dei vostri dati personali a soggetti terzi operanti nel settore 
 finanziario, editoriale, assicurativo, universitario, per loro proprie finalit� di marketing (e precisamente: 
 l�invio di materiale promozionale ed informativo, l�invio di inviti ad eventi ed iniziative da loro organizzate o
 gestite, il compimento di studi e ricerche di mercato). E� possibile consultare l�elenco dei soggetti terzi sopra 
 indicati mediante semplice richiesta scritta da inoltrarsi all�indirizzo mail : privacy@salonedelrisparmio.com. 
 Tali soggetti, in seguito alla comunicazione e/o cessione dei dati, agiranno in qualit� di Titolari del trattamento e 
 saranno tenuti ad inoltrare all�Interessato, in via preliminare all�esercizio di qualsiasi attivit� di marketing, apposita 
 informativa ex art. 13 Codice della Privacy, comunicando altres� l�origine dei dati personali.<br><br>Vi informiamo che, a 
 seguito della vostra registrazione, verrete muniti di un apposito tesserino di riconoscimento � badge dotato di barra magnetica. 
 Il badge, nominale e non cedibile, costituisce titolo per l�accesso e la partecipazione all�evento. Gli espositori presenti 
 all�evento saranno muniti di lettori ottici in grado di rilevare, dalla lettura della barra magnetica, se avete acconsentito o 
 meno alla comunicazione e/o cessione dei dati personali a terzi (finalit� di cui al punto 3.). Nel caso in cui accettiate di 
 sottoporre il vostro badge alla lettura ottica, e questo rilevi che avete prestato il consenso a tale finalit�, l�espositore 
 non acquisir� direttamente i vostri dati personali, ma acquisir� esclusivamente un codice identificativo anonimo che trasmetter�, 
 al termine dell�evento, ai Contitolari. I Contitolari assoceranno al codice il vostro nominativo e procederanno alla comunicazione
 e/o cessione dei vostri dati all�espositore. Vi precisiamo che l�acquisizione di tale codice identificativo (seppur anonimo) 
 da parte dell�espositore rappresenta trattamento dei dati personali e che, prestando consenso al trattamento ai fini di cui al 
 superiore punto n. 3 , acconsentirete a tale trattamento.<br><br>Vi precisiamo che i Contitolari possono trattare i vostri dati 
 personali per adempiere ad obblighi previsti dalla legge, da regolamenti o da norme comunitarie (finalit� per cui non � necessario 
 acquisire il Vostro specifico consenso).<br><br>Vi precisiamo altres� che, nell�ambito del perseguimento della finalit� indicata al 
 superiore punto 1. (Per consentire l�iscrizione e la partecipazione dell�Interessato all�evento �Il Salone del Risparmio � Ed. 2016),  
 i vostri dati personali potrebbero essere comunicati ai dipendenti dei Contitolari e/o del Responsabile del trattamento nonch� a persone, 
 enti, associazioni, che operano, per conto dei Contitolari e/o del Responsabile, per prestare assistenza e consulenza in materia legale, 
 organizzativa, informatica, contabile etc., e per realizzare servizi quali stampa, imbustamento, spedizione etc. Trattandosi di finalit� 
 dirette ad eseguire obblighi derivanti dalla Vostra richiesta di iscrizione e partecipazione all�evento �Il Salone del Risparmio � 
 Ed. 2016�, non si rende necessario, ai sensi dell�art. 24, lett. b) del Codice della Privacy, acquisire il Vostro specifico consenso. 
 In ogni caso, laddove la comunicazione dei dati personali alle categorie sopra individuate dovesse rendere necessario l�acquisizione 
 dello specifico consenso dell�Interessato, sar� cura dei Contitolari provvedervi a norma di legge.<br><br>Vi informiamo che le finalit� 
 di cui ai punti 2 e 3 sopra elencate saranno realizzate sia con modalit� automatizzate che non automatizzate; in particolare, in 
 relazione alle attivit� di marketing, queste saranno realizzate sia con strumenti tradizionali (es. posta ordinaria e telefono con 
 operatore etc.) che con strumenti automatizzati (es. email, sms, telefono senza operatore, fax etc.); pertanto l�eventuale consenso 
 che l�Interessato prester� si intende esteso ad entrambe le modalit� di invio evidenziate.<br><br>Natura obbligatoria o facoltativa 
 dei dati trattati<br>Vi informiamo che il consenso al trattamento dei dati per le finalit� di cui al numero 1. � necessario per consentire 
 l�effettiva possibilit� di iscrizione e partecipazione dell�Interessato all�evento �Il Salone del Risparmio � edizione 2016�. Il mancato 
 conferimento dei dati ai fini di cui al numero 1. render� pertanto impossibile l�iscrizione e la partecipazione dell�Interessato all�evento 
 �Il Salone del Risparmio � edizione 2016�. I dati strettamente necessari per la tale finalit� sono contrassegnati con il simbolo dell�asterisco 
 (*).<il>conferimento dei dati ai fini di cui ai numeri 2 e 3 � invece facoltativo ed il mancato conferimento non inficia la partecipazione 
 dell�interessato all�evento �Il Salone del Risparmio � edizione 2016�, precludendo soltanto la possibilit� dello svolgimento delle finalit� 
 descritte; inoltre il consenso � sempre revocabile mediante semplice richiesta da inoltrarsi all�indirizzo mail: privacy@salonedelrisparmio.
 com.</il><br><br>Diritti dell�Interessato (art. 7 Dlgs n.  196/2003)<br><br>Vi informiamo inoltre che Vi sono riconosciuti i diritti di cui 
 all�art. 7 del Codice della Privacy. In particolare, inviando una richiesta all�indirizzo e-mail del Responsabile del trattamento 
 (privacy@salonedelrisparmio.com) potrete conoscere i dati di cui sono in possesso i Contitolari; la loro origine ed il loro utilizzo; 
 potrete ottenerne l�aggiornamento, la rettificazione e, se vi � interesse, l�integrazione; potrete chiedere altres� la cancellazione, 
 la trasformazione in forma anonima o il blocco dei dati trattati in violazione della legge; potrete inoltre opporvi, per motivi legittimi, 
 al loro trattamento; l�opposizione  al trattamento dei Vostri dati personali � sempre possibile in caso di trattamento per finalit� pubblicitarie, 
 di vendita diretta, per il compimento di ricerche di mercato o comunicazione commerciale; si precisa che, con riferimento a tali ultime finalit�, 
 il diritto di opposizione al trattamento effettuato con modalit� automatizzate si estende anche alle modalit� tradizionali, ma resta salva in ogni 
 caso la facolt� dell�Interessato di esercitare tale diritto anche solo in parte, con riferimento alle sole modalit� automatizzate o alle sole modalit� 
 tradizionali. Resta inteso che la cancellazione e/o blocco dei dati contrassegnati con il simbolo asterisco (*) impedir� l�iscrizione e la partecipazione 
 all�evento �Il Salone del Risparmio � Ed. 2016�.<br><br>Titolare e Responsabile del trattamento<br><br>I Contitolari del trattamento dei dati personali 
 sono l�associazione Assogestioni ed Assogestioni Servizi Srl (rispettivamente  con sede legale a Roma in Via In Lucina n. 17 e a Milano in Via Andegari n. 18).
 <br><br>Responsabile del trattamento dei dati personali � OBJECT WAY  con sede in Milano. L�indirizzo mail del Responsabile cui rivolgere richieste di informazioni 
 in merito al trattamento dei propri dati personali nonch� presso cui esercitare i diritti di cui all�art. 7 Codice della Privacy �: 
 privacy@salonedelrisparmio.com. Sede del trattamento dei dati � la sede legale del Responsabile sopra individuata.<vi>informiamo da 
 ultimo che i Contitolari non trattano dati classificabili come sensibili ai sensi dell�art. 4, lettera d) del Codice Privacy 
 (dati idonei a rivelare l�origine razziale ed etnica, le convinzioni religiose, filosofiche o di altro genere, le opinioni 
 politiche o sindacali, lo stato di salute o la vita sessuale dell�interessato).</vi>';
 
 echo utf8_encode($str);
 ?>
 
 
							</div>
							
						</div>
					</div>
					<div style="width:100%;text-align:right;"><a href="privacy.php?barcode=<?php echo $barcode; ?>">CHIUDI</a></div>				
					<?php include("app/footer.php"); ?>
				</article>
			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			
			
			

	</body>
</html>
<?php 

$conn=null;
} else echo "<script>location.href = 'index.php';</script>";  ?>


