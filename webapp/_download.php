
<?php
include("app/connessione.php");
include("app/function.php");
if (isset( $_SESSION['company']['id'])) { ?>

<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<style>
		#nav {
			display:block;
		background-color: #282828;
		text-align: center;
		position: fixed;
		left: 0;
		top: 0;
		width: 100%;
		z-index: 10000;
		cursor: default;
	}

		#nav ul {
			margin-bottom: 0;
		}

		#nav li {
			display: inline-block;
		}

		#nav a {
			position: relative;
			display: block;
			color: #fff;
			text-decoration: none;
			outline: 0;
		}

			#nav a:hover {
				color: #fff !important;
			}

			#nav a.active:before {
				content: '';
				display: block;
				position: absolute;
				bottom: -0.6em;
				left: 50%;
				margin-left: -0.75em;
				border-left: solid 0.75em transparent;
				border-right: solid 0.75em transparent;
				border-top: solid 0.6em #282828;
			}
			.bbor {
				border:1px solid #000;
			}
		</style>
	</head>
	<body  style="background:#fff !important;width:100%;margin:0px;padding:0px">
	<!-- Nav -->
		<?php include("app/menu_sub.php"); ?>



		<!-- Work -->
			<div class="wrapper style2" style="margin-top:100px;background:#fff !important;">
				<article id="work">
					<header>
						<h2><?php echo  $_SESSION['company']['name']; ?></h2>
						
					</header>
					<div class="container">
						<div class="row"> 
						<table style="color:#000;" class="bbor" cellspacing="0">
						<tr style="background-color:#FAAC58;border:1px solid #000;">
							<td class="bbor">Barcode</td>
							<td class="bbor">Nome</td>
							<td class="bbor">Cognome</td>
							<td class="bbor">Categoria</td>
							
							<td class="bbor">Email</td>
							
							<td class="bbor">Città</td>
							<td class="bbor">Indirizzo</td>
							<td class="bbor">Provincia</td>
							<td class="bbor">CAP</td>
							<td class="bbor">Regione</td>
							<td class="bbor">Nazione</td>
							<td class="bbor">Recapito Cell.</td>
							<td class="bbor">Azienda</td>
							<td class="bbor">Sesso</td>
							<td class="bbor">Prodotto</td>
							<td class="bbor">Data</td>
							<td class="bbor">Ora</td>
						</tr>
						<?php
						if (isset( $_SESSION['company']['id'])) { 
							$sql = "SELECT *, guests.id as cc FROM `guests` left JOIN accordation on guests.id=accordation.guest_id inner JOIN product on accordation.product_id=product.id where accordation.company_id=".$_SESSION['company']['id']." and  accordation.attivo=1 order by guests.id ASC, product.id ASC" ;
							 $ps = $conn->query($sql);
							 if ($ps->rowCount() > 0) {
								 foreach($ps as $row){
									 
							$rr = explode(" ", $row['data']);
							$gg = explode("-", $rr[0]);
							$gg_agg = $gg[2]."/".$gg[1]."/".$gg[0];
							echo '<tr style="border:1px solid #000;">';
							echo "<td class='bbor' >".$row['barcode']."</td>";
							echo "<td class='bbor' >".ucwords($row['firstname'])."</td>";
							echo "<td class='bbor'>".ucwords($row['lastname'])."</td>";
							echo "<td class='bbor' >".ucwords($row['type'])."</td>";
							echo "<td class='bbor'>".$row['emailadress']."</td>";
							echo "<td class='bbor'>".strtoupper($row['city'])."</td>";
							echo "<td class='bbor'>".ucwords($row['street'])."</td>";
							echo "<td class='bbor'>".strtoupper($row['pr'])."</td>";
							echo "<td class='bbor'>".$row['zip']."</td>";
							echo "<td class='bbor'>".strtoupper($row['regione'])."</td>";
							echo "<td class='bbor'>".strtoupper($row['nation'])."</td>";
							echo "<td class='bbor'>".$row['mobile']."</td>";
							echo "<td class='bbor'>".strtoupper($row['company'])."</td>";
							echo "<td class='bbor'>".strtoupper($row['gender'])."</td>";	
							echo "<td class='bbor'>".$row['name']."</td>";
							echo "<td class='bbor'>".$gg_agg."</td>";	
							echo "<td class='bbor'>".$rr[1]."</td>";
							echo "</tr>"; 
									 
								 }
							 }	 
		
						}
						
						?>
						</table>	
							
						</div>
					</div>
					<?php include("app/footer.php"); ?>
				</article>
			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			

	</body>
</html>
<?php 
$conn = null;

} else echo "<script>location.href = 'index.php';</script>";  ?>

