<?php

include("app/connessione.php");
include("app/function.php");
if (isset( $_SESSION['company']['id'])) { ?>

<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	
	
	
	
	
	
	
	<body style="padding:0px;margin:0px auto;" onclick="document.memor.barcode.focus();"  onload="document.memor.barcode.focus();" onfocus="document.memor.barcode.focus();">

	
			<?php include("app/menu_app.php"); ?>


		<!-- Work -->
			<div class="wrapper style2" >
				<article id="work">
					<header>
						<h2><?php echo  $_SESSION['company']['name']; ?></h2>
						<h3>Leggi il barcode</h3>
						
					</header>
					
					<div class="container">
						<div class="row" style="padding-left:5%;padding-right:5%;">
						
							<div style="width:100%;margin:auto;">
							
							
							<form action="register.php" id="formmemor" method="post" name="memor" />
								<section class="box style2" >
									<div class="6u 12u(mobile)" style="width:100%">
									<?php
									if(isset($_GET['errore'])) {
										$msg = "";
										if ($_GET['errore'] == 1) $msg = "ERRORE PRIVACY";
										else if ($_GET['errore'] == 3) $msg = "BARCODE NON VALIDO";
										else if ($_GET['errore'] == 4) $msg = "ERRORE DI SISTEMA";
										else if ($_GET['errore'] == 5) $msg = "ERRORE DI SISTEMA";
										else if ($_GET['errore'] == 6) $msg = "TESTO PRIVACY NON ACCETTATO";
										else $msg = "ERRORE DI SISTEMA";
										
										echo "<div class='error clos' >".$msg."<br>Riprova</div>";
									}
									if(isset($_GET['success'])) {
										echo "<div class='successo clos' style='color:green;' >Azioni/Prodotti registrati con successo</div>";
										
									}
									?>
									
												<input class="spazio" type="text" name="barcode" id="p1" placeholder="barcode" autofocus >
												
												
											</div>
									<input type="submit" name="read" class="button small scrolly" value="conferma"/>
									
								</section>
								</form>
							
							</div>
						</div>
					</div>
					
					<?php include("app/footer.php"); ?>
				</article>
			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			
			
			<script>
			$(document).keypress(
		      function(event){
				//alert(event.which);
		       if (event.which == '13') {
		          event.preventDefault();
				 var x = $('#p1').val();
				
				//  $( "#formmemor" ).serialize();
		          $( "#formmemor" ).submit();
		        }


		  });
			setTimeout(function(){ $('.clos').slideUp("slow"); }, 1000);
			</script>

	</body>
</html>
<?php 

$conn =null;

} else echo "<script>location.href = 'index.php';</script>";  ?>