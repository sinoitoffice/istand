<?php
include("app/connessione.php");
include("app/function.php");
if (isset( $_SESSION['company']['id'])) { 

include("app/app.php");

?>

<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>

		<!-- Nav -->
			<?php include("app/menu_sub.php"); ?>



		<!-- Work -->
			<div class="wrapper style2" style="margin-top:50px;">
				<article id="work">
					<header class="wec">
						<h2><?php echo  $_SESSION['company']['name']; ?></h2>
						<h3>Configurazione prodotti</h3>
						
					</header>
					<form action="" method="post"/>
					<div class="container">
						<div class="row">
						
							<div class="4u 12u(mobile) product">
								<section class="box style1">
								
									<div class="6u 12u(mobile)" style="margin:auto;">
										<?php
										if ($err == 2) echo '<div class="success" >Prodotti Salvati Correttamente</div>';
										else if ($err == 1) echo "<div class='error' >C'è stato un'erroe<br>Contatta l'assistenza </div>";					
 else if ($err == 3) echo "<div class='error' >Nessun inserimento possibile</div>";

									?>		
									
									<?php
									$sql = "select * from product where company_id =".$_SESSION['company']['id'];
									
									$n = 1;
									$ps = $conn->query($sql);
									if ($ps->rowCount() > 0) {
										 foreach ($ps as $row) {
											 $disabled = "";
											 if ($row['name'] == "") {
													$nm = "Prodotto ".$n;
													$val = "";
													
											 }
											 else  {
												 $nm = $row['name'];
												 $val = $row['name'];
												 $disabled = "disabled";
											 }
											 
											echo '<input type="text" name="input_text['.$row["id"].']" id="p'.$n.'" value ="'.$val.'" placeholder="'.$nm.'" class="spazio" '.$disabled.' >';
										    $n++;	
										}
									}
																
									?>
									
									
									
												
											</div>
									<input type="submit" name="registra" class="button small scrolly" />
									
								</section>
							</div>
							
						</div>
					</div>
					</form>
					<?php include("app/footer.php"); ?>
				</article>
			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			

	</body>
</html>
<?php 

$conn =null;

} else echo "<script>location.href = 'index.php';</script>";  ?>
