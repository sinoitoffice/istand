<?php
session_start();
session_destroy();

?>
<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js?<?php echo time(); ?>"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css?<?php echo time(); ?>" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css?<?php echo time(); ?>" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css?<?php echo time(); ?>" /><![endif]-->
		
		

		
		
		
	</head>
	<body style="padding:0px;margin:0px auto;">

	
			<nav id="nav">
				<ul class="container">
					<li><a href="" style="cursor:none;"></a></li>
					<li><a href="" style="cursor:none;"></a></li>
					<li><a href="" style="cursor:none;"></a></li>
					<li><a href="" style="cursor:none;"></a></li>
				</ul>
				
			</nav>


		<!-- Work -->
			<div class="wrapper style2">
				<article id="work">
					<header>
						<h2>Salone del Risparmio 2016 WebApp</h2>
						<h3>Login</h3>
						
					</header>
					
					<div class="container">
						<div class="row" style="padding-left:5%;padding-right:5%;">
						
							<div style="width:100%;margin:auto;">
							<form action="login.php" method="post"/>
								<section class="box style2" style="width:100%" >
									<div class="6u 12u(mobile)" style="width:100%">
									<?php
									if(isset($_GET['error'])) {
										$msg = "";
										if ($_GET['error'] == 1) $msg = "Login Fallito";
										
										
										echo "<div class='error' >".$msg."<br>Riprova</div>";
									}
									
									?>
												<input class="spazio" type="text" name="p1" id="p1" placeholder="username">
												<input class="spazio" type="password" name="p2" id="p2" placeholder="password">
												
											</div>
									<input type="submit" name="login" class="button small scrolly" value="login"/>
									
								</section>
								</form>
							</div>
							
						</div>
					</div>
					
					<?php include("app/footer.php"); ?>
				</article>
			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js?<?php echo time(); ?>"></script>
			
		
			
	

	</body>
</html>