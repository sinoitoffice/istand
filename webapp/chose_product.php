<?php

include("app/connessione.php");
include("app/function.php");

if (isset($_SESSION['company']['id'])) { 

	$company_id= intval($_SESSION['company']['id']);
	if (isset($_SESSION['guest_id'])) $id = intval($_SESSION['guest_id']);
	else echo "<script>location.href = 'home.php?errore=2';</script>";


?>
<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/style.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		
	</head>
	<body>

	
		<?php include("app/menu_app.php"); ?>

		<!-- Work -->
			<div class="wrapper style2">
				<article id="work">
					<header>
						<h2><?php echo  $_SESSION['company']['name']; ?></h2>
						
						
					</header>
							<div class="container">
						<div class="row" style="padding-left:5%;padding-right:5%;">
						
							<div style="width:100%;margin:auto;">
							<div class="4u 12u(mobile)" style="width:50%;float:right;">	
								<section class="box style2" >
									
									<div class="6u 12u(mobile)" style="width:100%">
											<div class="6u 12u(mobile)" style="width:100%;text-align:center;">
											<?php
											 $ids = bottone_rosa($conn, $company_id, $id);
											
											
											$sql = "select * from product where company_id =".$company_id." and name != ''";
											
											
											$ps = $conn->query($sql);
											if ($ps->rowCount() > 0) {
												foreach ($ps as $row) {
													if(in_array($row['id'], $ids)) {
														echo '<button type="button" name="login" class="btna btna-pushed" value="'.$row['id'].'" style="width:100%;">';
													echo '<i id="o'.$row['id'].'" class="fa fa-toggle-off" style="display:none"></i><i id="c'.$row['id'].'" class="fa fa-toggle-on" style="display:block"></i>';
													echo $row['name'];
													echo '</button>';		
														
													} else {
													
													
													
													
													echo '<button type="button" name="login" class="btna" value="'.$row['id'].'" style="width:100%;" >';
													echo '<i id="o'.$row['id'].'" class="fa fa-toggle-off"></i><i id="c'.$row['id'].'" class="fa fa-toggle-on" style="display:none"></i>';
													echo $row['name'];
													echo '</button>';		
												}
												}												
											}									
											
											?>
											</div>											
										</div>								
							</div>
							<div class="4u 12u(mobile)" style="width:45%">							
								<section class="box style2" style="margin-bottom:20px;">
									<div class="6u 12u(mobile)" style="width:100%">
											<div class="6u 12u(mobile)" style="width:100%;text-align:left;">
												<?php
												$sql_g = "select * from guests where id =".$id;
												$ps = $conn->query($sql_g);
												if ($ps->rowCount() > 0) {
													foreach ($ps as $row) {
														$nome = $row['firstname'];
														$cogn = $row['lastname'];
														$type = $row['type'];
														$profile = $row['profile'];
														$category = $row['category'];
														$emailadress = $row['emailadress'];
														$barcode = $row['barcode'];
														
													}
												}
												?>
												<p><strong>Barcode: <?php echo $barcode; ?></strong></p>
												<hr />
												<p><strong>Nome: <?php echo $nome; ?></strong></p>
												<p><strong>Cognome: <?php echo $cogn; ?></strong></p>
												<p><strong>Email: <?php echo $emailadress; ?></strong></p>
												<p><strong>Categoria: <?php echo $type; ?></strong></p>
											</div>											
										</div>	
									
								</section>	
								<section class="box style2" >
									<div class="6u 12u(mobile)" style="width:100%">
											<div class="6u 12u(mobile)" style="width:100%;text-align:center;">
												<a href="home.php?success=0" class="button small scrolly"  >HOME >> </a>
											</div>											
										</div>								
								</section>	
									

										
							</div>
							
							
							
							</div>
						</div>
					</div>
					
					<?php include("app/footer.php"); ?>
				</article>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
		
			
			<script src="assets/js/jquery.bpopup.min.js"></script>
			<script>
			$('.btna').on({ 'touchstart' : function(event){
				var clikato = $(this);
				var x = $(this).val();
				var y = <?php echo  $id; ?>;
				$.ajax({
					type: "POST",
					url: "ajaxCall.php",
					data: "product_id="+x+"&guest_id="+y,
					success: function(risposta){	
							if (risposta == 2) {
								clikato.css({"background-color":"#c63939"});
								clikato.addClass('btna-pushed');
								$('#o'+x).hide();
								$('#c'+x).show();

							}	else if (risposta == 1) {
								clikato.css({"background-color":"#43B3E0"});
								clikato.removeClass('btna-pushed');
								$('#o'+x).show();
								$('#c'+x).hide();

							}	else alert("C'è stato qualche errore. Contatta l'assistenza")				
						},					
							error: function(){
								setTimeout(function(){ alert("Server Error"); }, 300);
							}
				});
			 } });
			
			</script>

	</body>
</html>
<?php 
$conn = null;
} else echo "<script>location.href = 'index.php';</script>"; ?>