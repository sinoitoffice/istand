<?php
include("app/connessione.php");
include("app/function.php");

if (isset($_SESSION['company']['id'])) { 
	if (isset($_GET['barcode'])) $barcode = $_GET['barcode'];
	else $barcode = "";

?>

<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Salone del Risparmio 2016</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="assets/css/jquery.modal.css" type="text/css" media="screen" />
		<style>
		.modal a.close-modal {
   
   display:none;
}
		</style>
		
	</head>
	<body>
<div id="ex1" style="display:none;width:80%;overflow: auto;height: 70%;background-image: url(assets/css/images/bg.png);">
	<div style="width:90%">
	<a href="#" rel="modal:close" style="display_block;width:100%;text-align:right;"><div class="xxx"></div></a>
	<br><br>
	<div style="text-align:center"><h2>Finalità e modalità del trattamento dei dati personali</h2></div>
			<p style="text-align:justify;">
			Finalità e modalità del trattamento dei dati personali<br><br>Ai sensi dell’art. 13 del DLGS n. 
			196/2003 (c.d. Codice della Privacy), nella Vostra qualità di Interessati al trattamento, Vi informiamo
			che i dati personali da Voi forniti nella scheda di registrazione saranno trattati da Assogestioni e 
			da Assogestioni Servizi Srl, (rispettivamente  con sede legale a Roma in Via In Lucina n. 17 e a Milano
			in Via Andegari n. 18), in qualità di contitolari del trattamento, con modalità informatiche e/o cartacee 
			e comunque in modo da garantire la sicurezza, la protezione e la segretezza dei Vostri dati,  per le seguenti
			finalità:<br><br>1)	per consentire l’iscrizione e la partecipazione dell’Interessato all’evento “Il Salone 
			del Risparmio – Ed. 2016” e ad eventi ed incontri connessi a detta manifestazione;<br>2)	previo specifico 
			consenso dell’Interessato, per consentire ai Contitolari la realizzazione di finalità di marketing, ivi incluse 
			l’invio di materiale promozionale ed informativo; l’invio delle newsletter relative all’evento “Il Salone del 
			Risparmio – Ed. 2016”; l’invio di inviti ad eventi ed iniziative dei contitolari o da questi gestite; il 
			compimento di studi e ricerche di mercato.<br>3)	previo specifico consenso dell’Interessato, per consentire 
			ai Contitolari la comunicazione e/o la cessione dei vostri dati personali a soggetti terzi operanti nel settore 
			finanziario, editoriale, assicurativo, universitario, per loro proprie finalità di marketing (e precisamente: 
			l’invio di materiale promozionale ed informativo, l’invio di inviti ad eventi ed iniziative da loro organizzate o
			gestite, il compimento di studi e ricerche di mercato). E’ possibile consultare l’elenco dei soggetti terzi sopra 
			indicati mediante semplice richiesta scritta da inoltrarsi all’indirizzo mail : privacy@salonedelrisparmio.com. 
			Tali soggetti, in seguito alla comunicazione e/o cessione dei dati, agiranno in qualità di Titolari del trattamento e 
			saranno tenuti ad inoltrare all’Interessato, in via preliminare all’esercizio di qualsiasi attività di marketing, apposita 
			informativa ex art. 13 Codice della Privacy, comunicando altresì l’origine dei dati personali.<br><br>Vi informiamo che, a 
			seguito della vostra registrazione, verrete muniti di un apposito tesserino di riconoscimento – badge dotato di barra magnetica. 
			Il badge, nominale e non cedibile, costituisce titolo per l’accesso e la partecipazione all’evento. Gli espositori presenti 
			all’evento saranno muniti di lettori ottici in grado di rilevare, dalla lettura della barra magnetica, se avete acconsentito o 
			meno alla comunicazione e/o cessione dei dati personali a terzi (finalità di cui al punto 3.). Nel caso in cui accettiate di 
			sottoporre il vostro badge alla lettura ottica, e questo rilevi che avete prestato il consenso a tale finalità, l’espositore 
			non acquisirà direttamente i vostri dati personali, ma acquisirà esclusivamente un codice identificativo anonimo che trasmetterà, 
			al termine dell’evento, ai Contitolari. I Contitolari assoceranno al codice il vostro nominativo e procederanno alla comunicazione
			e/o cessione dei vostri dati all’espositore. Vi precisiamo che l’acquisizione di tale codice identificativo (seppur anonimo) 
			da parte dell’espositore rappresenta trattamento dei dati personali e che, prestando consenso al trattamento ai fini di cui al 
			superiore punto n. 3 , acconsentirete a tale trattamento.<br><br>Vi precisiamo che i Contitolari possono trattare i vostri dati 
			personali per adempiere ad obblighi previsti dalla legge, da regolamenti o da norme comunitarie (finalità per cui non è necessario 
			acquisire il Vostro specifico consenso).<br><br>Vi precisiamo altresì che, nell’ambito del perseguimento della finalità indicata al 
			superiore punto 1. (Per consentire l’iscrizione e la partecipazione dell’Interessato all’evento “Il Salone del Risparmio – Ed. 2016),  
			i vostri dati personali potrebbero essere comunicati ai dipendenti dei Contitolari e/o del Responsabile del trattamento nonché a persone, 
			enti, associazioni, che operano, per conto dei Contitolari e/o del Responsabile, per prestare assistenza e consulenza in materia legale, 
			organizzativa, informatica, contabile etc., e per realizzare servizi quali stampa, imbustamento, spedizione etc. Trattandosi di finalità 
			dirette ad eseguire obblighi derivanti dalla Vostra richiesta di iscrizione e partecipazione all’evento “Il Salone del Risparmio – 
			Ed. 2016”, non si rende necessario, ai sensi dell’art. 24, lett. b) del Codice della Privacy, acquisire il Vostro specifico consenso. 
			In ogni caso, laddove la comunicazione dei dati personali alle categorie sopra individuate dovesse rendere necessario l’acquisizione 
			dello specifico consenso dell’Interessato, sarà cura dei Contitolari provvedervi a norma di legge.<br><br>Vi informiamo che le finalità 
			di cui ai punti 2 e 3 sopra elencate saranno realizzate sia con modalità automatizzate che non automatizzate; in particolare, in 
			relazione alle attività di marketing, queste saranno realizzate sia con strumenti tradizionali (es. posta ordinaria e telefono con 
			operatore etc.) che con strumenti automatizzati (es. email, sms, telefono senza operatore, fax etc.); pertanto l’eventuale consenso 
			che l’Interessato presterà si intende esteso ad entrambe le modalità di invio evidenziate.<br><br>Natura obbligatoria o facoltativa 
			dei dati trattati<br>Vi informiamo che il consenso al trattamento dei dati per le finalità di cui al numero 1. è necessario per consentire 
			l’effettiva possibilità di iscrizione e partecipazione dell’Interessato all’evento “Il Salone del Risparmio – edizione 2016”. Il mancato 
			conferimento dei dati ai fini di cui al numero 1. renderà pertanto impossibile l’iscrizione e la partecipazione dell’Interessato all’evento 
			“Il Salone del Risparmio – edizione 2016”. I dati strettamente necessari per la tale finalità sono contrassegnati con il simbolo dell’asterisco 
			(*).<il>conferimento dei dati ai fini di cui ai numeri 2 e 3 è invece facoltativo ed il mancato conferimento non inficia la partecipazione 
			dell’interessato all’evento “Il Salone del Risparmio – edizione 2016”, precludendo soltanto la possibilità dello svolgimento delle finalità 
			descritte; inoltre il consenso è sempre revocabile mediante semplice richiesta da inoltrarsi all’indirizzo mail: privacy@salonedelrisparmio.
			com.</il><br><br>Diritti dell’Interessato (art. 7 Dlgs n.  196/2003)<br><br>Vi informiamo inoltre che Vi sono riconosciuti i diritti di cui 
			all’art. 7 del Codice della Privacy. In particolare, inviando una richiesta all’indirizzo e-mail del Responsabile del trattamento 
			(privacy@salonedelrisparmio.com) potrete conoscere i dati di cui sono in possesso i Contitolari; la loro origine ed il loro utilizzo; 
			potrete ottenerne l’aggiornamento, la rettificazione e, se vi è interesse, l’integrazione; potrete chiedere altresì la cancellazione, 
			la trasformazione in forma anonima o il blocco dei dati trattati in violazione della legge; potrete inoltre opporvi, per motivi legittimi, 
			al loro trattamento; l’opposizione  al trattamento dei Vostri dati personali è sempre possibile in caso di trattamento per finalità pubblicitarie, 
			di vendita diretta, per il compimento di ricerche di mercato o comunicazione commerciale; si precisa che, con riferimento a tali ultime finalità, 
			il diritto di opposizione al trattamento effettuato con modalità automatizzate si estende anche alle modalità tradizionali, ma resta salva in ogni 
			caso la facoltà dell’Interessato di esercitare tale diritto anche solo in parte, con riferimento alle sole modalità automatizzate o alle sole modalità 
			tradizionali. Resta inteso che la cancellazione e/o blocco dei dati contrassegnati con il simbolo asterisco (*) impedirà l’iscrizione e la partecipazione 
			all’evento “Il Salone del Risparmio – Ed. 2016”.<br><br>Titolare e Responsabile del trattamento<br><br>I Contitolari del trattamento dei dati personali 
			sono l’associazione Assogestioni ed Assogestioni Servizi Srl (rispettivamente  con sede legale a Roma in Via In Lucina n. 17 e a Milano in Via Andegari n. 18).
			<br><br>Responsabile del trattamento dei dati personali è OBJECT WAY  con sede in Milano. L’indirizzo mail del Responsabile cui rivolgere richieste di informazioni 
			in merito al trattamento dei propri dati personali nonché presso cui esercitare i diritti di cui all’art. 7 Codice della Privacy è: 
			privacy@salonedelrisparmio.com. Sede del trattamento dei dati è la sede legale del Responsabile sopra individuata.<vi>informiamo da 
			ultimo che i Contitolari non trattano dati classificabili come sensibili ai sensi dell’art. 4, lettera d) del Codice Privacy 
			(dati idonei a rivelare l’origine razziale ed etnica, le convinzioni religiose, filosofiche o di altro genere, le opinioni 
			politiche o sindacali, lo stato di salute o la vita sessuale dell’interessato).
			<br><br>
			<!--<a href="#" rel="modal:close" style="display_block;width:100%;text-align:right;"><div class="xxx"></div></a>-->
		</p>
	</div>
</div>
	
			<?php include("app/menu_app.php"); ?>
<!-- testo -->





<!-- fine -->



		<!-- Work -->
			<div class="wrapper style2">
				<article id="work">
					<header>
						<h2><?php echo  $_SESSION['company']['name']; ?></h2>					
					</header>
					
					<div class="container">
						<div class="row" >
							<div class="4u 12u(mobile)" style="width:100%">
							
								<section class="box style2" >
									<div class="6u 12u(mobile)" style="width:100%">
												Il partecipante non ha dato il consenso al trattamento dei dati.
												<br><br>
											
												S'intende dare il  consenso 
												al trattamento dei dati personali per le finalità di cui al punto 3.<br><br>
												<!--<a href="pricavy_txt.php?barcode=<?php echo $barcode; ?>" onclick="newWindow(this.href, 'popup', 600, 500, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank">Leggi l'informativa</a>-->
												<a href="#ex1" rel="modal:open" >Leggi l'informativa</a>
												
												
											</div>
											<br><br>
											
									<table style="width:100%">
										<tr>
											<td  style="width:45%">
											
												<form action="modify.php" method="post"/>
												<input type="submit" name="si" class="button small scrolly" value="ACCONSENTE" style="width:100%;"/>
												<input type="hidden" name="barcode" value="<?php echo  $barcode; ?>" />
												</form>
											</td>
											<td style="width:10%"></td>
											<td style="width:45%">
									
												<form action="home.php?errore=6" method="post" />
												<input type="submit" name="no" class="button small scrolly" value="NON ACCONSENTE" style="width:100%;"/>
												</form>
											<td>
										</tr>
									</table>
									
								</section>
							
							</div>
							
						</div>
					</div>
					
					<?php include("app/footer.php"); ?>
				</article>
			</div>



		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
		
			<script type="text/javascript">
			function newWindow(a_str_windowURL, a_str_windowName, a_int_windowWidth, a_int_windowHeight, a_bool_scrollbars, a_bool_resizable, a_bool_menubar, a_bool_toolbar, a_bool_addressbar, a_bool_statusbar, a_bool_fullscreen) {
			  var int_windowLeft = (screen.width - a_int_windowWidth) / 2;
			  var int_windowTop = (screen.height - a_int_windowHeight) / 2;
			  var str_windowProperties = 'height=' + a_int_windowHeight + ',width=' + a_int_windowWidth + ',top=' + int_windowTop + ',left=' + int_windowLeft + ',scrollbars=' + a_bool_scrollbars + ',resizable=' + a_bool_resizable + ',menubar=' + a_bool_menubar + ',toolbar=' + a_bool_toolbar + ',location=' + a_bool_addressbar + ',statusbar=' + a_bool_statusbar + ',fullscreen=' + a_bool_fullscreen + '';
			  var obj_window = window.open(a_str_windowURL, a_str_windowName, str_windowProperties)
				if (parseInt(navigator.appVersion) >= 4) {
				  obj_window.window.focus();
				}
			}
				</script>
			

	</body>
</html>
<?php 

$conn = null;
} else echo "<script>location.href = 'index.php';</script>";  ?>