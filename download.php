<?php



		include("webapp/app/connessione.php");
		
		if (isset( $_SESSION['company']['id'])) { 
		$sql = "SELECT *, guests.id as cc FROM `guests` left JOIN accordation on guests.id=accordation.guest_id inner JOIN product on accordation.product_id=product.id where accordation.company_id=".$_SESSION['company']['id']." and  accordation.attivo=1 order by guests.id ASC, product.id ASC" ;
		$stylecolor = "DEDEDE";
		//date_default_timezone_set('Asia/Kolkata');
		require_once 'PHPExcel/Classes/PHPExcel.php';
		$tim = date("d/m/Y H.i.s", time());
		$filename = 'userReport'; //your file name
		$objPHPExcel = new PHPExcel();
		/*********************Add column headings START**********************/
			$objPHPExcel->setActiveSheetIndex(0) 
					->setCellValue('A1', 'Azienda: '. $_SESSION['company']['name'])
					->setCellValue('A2', 'Data scarico: '.$tim);
					
					$objPHPExcel->setActiveSheetIndex(0) 
					->setCellValue('A1', 'Azienda: ' . $_SESSION['company']['name'])
					->setCellValue('B1', '')
					->setCellValue('C1', '')
					->setCellValue('D1', '')
					->setCellValue('E1', '')
					->setCellValue('F1', '')
					->setCellValue('G1', '')
					->setCellValue('H1', '')
					->setCellValue('I1', '')
					->setCellValue('J1', '')
					->setCellValue('K1', '')
					->setCellValue('L1', '')
					->setCellValue('M1', '')
					->setCellValue('N1', '')
					->setCellValue('O1', '')
					->setCellValue('P1', '')
					->setCellValue('Q1', '');
					
					
					$objPHPExcel->setActiveSheetIndex(0) 
					->setCellValue('A2',  'Data scarico: '.$tim)
					->setCellValue('B2', '')
					->setCellValue('C2', '')
					->setCellValue('D2', '')
					->setCellValue('E2', '')
					->setCellValue('F2', '')
					->setCellValue('G2', '')
					->setCellValue('H2', '')
					->setCellValue('I2', '')
					->setCellValue('J2', '')
					->setCellValue('K2', '')
					->setCellValue('L2', '')
					->setCellValue('M2', '')
					->setCellValue('N2', '')
					->setCellValue('O2', '')
					->setCellValue('P2', '')
					->setCellValue('Q2', '');
					
		
		
		
		
		
		
		$objPHPExcel->setActiveSheetIndex(0) 
					->setCellValue('A3', 'Barcode')
					->setCellValue('B3', 'Nome')
					->setCellValue('C3', 'Cognome')
					->setCellValue('D3', 'Categoria')
					->setCellValue('E3', 'Email')
					->setCellValue('F3', 'Città')
					->setCellValue('G3', 'Indirizzo')
					->setCellValue('H3', 'Provincia')
					->setCellValue('I3', 'CAP')
					->setCellValue('J3', 'Regione')
					->setCellValue('K3', 'Nazione')
					->setCellValue('L3', 'Recapito Cell.')
					->setCellValue('M3', 'Azienda')
					->setCellValue('N3', 'Sesso')
					->setCellValue('O3', 'Prodotto')
					->setCellValue('P3', 'Data')
					->setCellValue('Q3', 'Ora');
		/*********************Add column headings END**********************/
		
		// You can add this block in loop to put all ur entries.Remember to change cell index i.e "A2,A3,A4" dynamically 
		/*********************Add data entries START**********************/
		$ps = $conn->query($sql);
		$n = 4;
			if ($ps->rowCount() > 0) {
			
			 $id_contr = 0;
			  foreach($ps as $row){
						
							$rr = explode(" ", $row['data']);
							$gg = explode("-", $rr[0]);
							$gg_agg = $gg[2]."/".$gg[1]."/".$gg[0];
							$objPHPExcel->setActiveSheetIndex(0) 
								->setCellValue('A'.$n, $row['barcode'])
								->setCellValue('B'.$n, ucwords($row['firstname']))
								->setCellValue('C'.$n, ucwords($row['lastname']))
								->setCellValue('D'.$n, ucwords($row['type']))
								->setCellValue('E'.$n, $row['emailadress'])
								->setCellValue('F'.$n, strtoupper($row['city']))
								->setCellValue('G'.$n, ucwords($row['street']))
								->setCellValue('H'.$n, strtoupper($row['pr']))
								->setCellValue('I'.$n, $row['zip'])
								->setCellValue('J'.$n, strtoupper($row['regione']))
								->setCellValue('K'.$n, strtoupper($row['nation']))
								->setCellValue('L'.$n, '="'.$row['mobile'].'"')
								->setCellValue('M'.$n, strtoupper($row['company']))
								->setCellValue('N'.$n, strtoupper($row['gender']))
								->setCellValue('O'.$n, $row['name'])
								->setCellValue('P'.$n, $gg_agg)
								->setCellValue('Q'.$n,  $rr[1]);
								
								
						
								
								
						
							
						
						$n++;
						$id_contr = $row['cc'];
			  }
		}
		
		
		/*********************Add data entries END**********************/
		
		/*********************Autoresize column width depending upon contents START**********************/
        foreach(range('A','Q') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}
		/*********************Autoresize column width depending upon contents END***********************/
		
		$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true); //Make heading font bold
		
		/*********************Add color to heading START**********************/
		$objPHPExcel->getActiveSheet()
					->getStyle('A1')
					->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()
					->setARGB('99ff99');
			$objPHPExcel->getActiveSheet()
					->getStyle('A3:Q3')
					->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()
					->setARGB('99ff99');
		/*********************Add color to heading END***********************/
		
		$objPHPExcel->getActiveSheet()->setTitle('userReport'); //give title to sheet
		$objPHPExcel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header("Content-Disposition: attachment;Filename=$filename.xls");
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
} else  echo "<script>location.href = 'index.php';</script>";
?>